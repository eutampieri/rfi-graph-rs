fn main() {
    println!("cargo:rerun-if-changed=orari-rfi/trains.json");
    let compressed = deflate::deflate_bytes(&std::fs::read("orari-rfi/trains.json").unwrap());
    std::fs::write("../target/trains.json.gz", compressed).unwrap();
}
