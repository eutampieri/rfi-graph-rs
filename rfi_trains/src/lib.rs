use serde::Deserialize;

#[derive(Debug, Clone)]
pub enum TrainNumber {
    Regionale {
        number: u32,
    },
    RegionaleVeloce {
        number: u32,
    },
    InterCity {
        number: u32,
    },
    FrecciaRossa {
        number: u32,
    },
    FrecciaArgento {
        number: u32,
    },
    FrecciaBianca {
        number: u32,
    },
    InterCityNotte {
        number: u32,
    },
    EuroNight {
        number: u32,
    },
    EuroCity {
        number: u32,
    },
    Italo {
        number: u32,
    },
    LeonardoExpress {
        number: u32,
    },
    TropeaExpress {
        number: u32,
    },
    RailJet {
        number: u32,
    },
    NightJet {
        number: u32,
    },
    Metropolitano {
        number: u32,
    },
    MalpensaExpress {
        number: u32,
    },
    RegioExpress {
        number: u32,
    },
    Espresso {
        number: u32,
    },
    TGV {
        number: u32,
    },
    Bus {
        number: String,
    },
    ServizioFerroviarioMetropolitanoTorino {
        line: u8,
        number: u32,
    },
    Suburbano {
        line: u8,
        number: u32,
    },
    /// Unknown train, the second field is the train type as returned from the API
    Unknown {
        number: u32,
        name: String,
    },
}

#[derive(Clone, Debug)]
pub enum Carrier {
    Trenitalia,
    Trenord,
    TrenitaliaTPER,
    NuovoTrasportoViaggiatori,
    SAD,
    ÖsterreichischeBundesbahnen,
    SociétéNationaleCheminsDeFer,
    FerrovieDelGargano,
    Sangritana,
    SlovenskeŽeleznice,
    FerrovieDelSudEst,
    SistemiTerritoriali,
    TrentinoTrasporti,
    EnteAutonomoVolturno,
    BernLötschbergSimplon,
    FerrovieApuloLucane,
    Busitalia,
    FerrovieUdineCividale,
    SwissFederalRailways,
    Unknown(String),
}

#[derive(Deserialize)]
pub struct RawTrainTrip {
    pub number: (String, String),
    pub carrier: String,
    pub stops: Vec<(String, String, bool)>,
    pub regularity: String,
    pub notices: String,
}

pub struct TrainTripStop {
    pub station_name: String,
    /// Time of departure since 00:00
    pub time: std::time::Duration,
    pub reduced_mobility: bool,
}

pub struct TrainTrip {
    pub number: TrainNumber,
    pub carrier: Carrier,
    pub stops: Vec<TrainTripStop>,
    pub regularity: String,
    pub notices: String,
}

impl From<RawTrainTrip> for TrainTrip {
    fn from(r: RawTrainTrip) -> Self {
        Self {
            number: r.number.into(),
            carrier: r.carrier.into(),
            stops: r.stops.into_iter().map(|x| x.into()).collect(),
            regularity: r.regularity,
            notices: r.notices,
        }
    }
}

impl From<(String, String)> for TrainNumber {
    fn from((t, n): (String, String)) -> Self {
        match t.as_str().to_lowercase().as_str() {
            "images/trn_bus.gif" => Self::Bus { number: n },
            "images/trn_r.gif" => Self::Regionale {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_rv.gif" => Self::RegionaleVeloce {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_ic.gif" => Self::InterCity {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_icn.gif" => Self::InterCityNotte {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_en.gif" => Self::EuroNight {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_fm1.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 1,
            },
            "images/trn_fm2.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 2,
            },
            "images/trn_fm3.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 3,
            },
            "images/trn_fm4.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 4,
            },
            "images/trn_fm5.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 5,
            },
            "images/trn_fm6.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 6,
            },
            "images/trn_fm7.gif" => Self::ServizioFerroviarioMetropolitanoTorino {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 7,
            },
            "images/trn_s1.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 1,
            },
            "images/trn_s2.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 2,
            },
            "images/trn_s3.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 3,
            },
            "images/trn_s4.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 4,
            },
            "images/trn_s5.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 5,
            },
            "images/trn_s6.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 6,
            },
            "images/trn_s7.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 7,
            },
            "images/trn_s8.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 8,
            },
            "images/trn_s9.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 9,
            },
            "images/trn_s10.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 10,
            },
            "images/trn_s11.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 11,
            },
            "images/trn_s12.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 12,
            },
            "images/trn_s13.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 13,
            },
            "images/trn_s14.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 14,
            },
            "images/trn_s40.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 40,
            },
            "images/trn_s50.gif" => Self::Suburbano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                line: 50,
            },
            "images/trn_ntv.gif" => Self::Italo {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_fb_m.png" => Self::FrecciaBianca {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_fav_m.png" => Self::FrecciaArgento {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_frv_m.png" => Self::FrecciaRossa {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_leo.gif" => Self::LeonardoExpress {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_txp.gif" => Self::TropeaExpress {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_rj.png" => Self::RailJet {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_m.gif" => Self::Metropolitano {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_ec.gif" => Self::EuroCity {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_mxp.gif" => Self::MalpensaExpress {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_re.gif" => Self::RegioExpress {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_nj.png" => Self::NightJet {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_e.gif" => Self::Espresso {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            "images/trn_tgv.gif" => Self::TGV {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
            },
            _ => Self::Unknown {
                number: n.split("/").nth(0).unwrap().parse().unwrap(),
                name: dbg!(t),
            },
        }
    }
}

impl From<String> for Carrier {
    fn from(raw_carrier: String) -> Self {
        match raw_carrier.as_str() {
            "TI" => Self::Trenitalia,
            "TTX" => Self::TrenitaliaTPER,
            "TN" => Self::Trenord,
            "ITA" => Self::NuovoTrasportoViaggiatori,
            "SAD" => Self::SAD,
            "ÖBB" => Self::ÖsterreichischeBundesbahnen,
            "OBB" => Self::ÖsterreichischeBundesbahnen,
            "SNCF" => Self::SociétéNationaleCheminsDeFer,
            "FdG" => Self::FerrovieDelGargano,
            "FAS" => Self::Sangritana,
            "SZ-PP" => Self::SlovenskeŽeleznice,
            "FSE" => Self::FerrovieDelSudEst,
            "ST" => Self::SistemiTerritoriali,
            "TT" => Self::TrentinoTrasporti,
            "EAV" => Self::EnteAutonomoVolturno,
            "RFF" => Self::SociétéNationaleCheminsDeFer,
            "BLS" => Self::BernLötschbergSimplon,
            "FAL" => Self::FerrovieApuloLucane,
            "BI" => Self::Busitalia,
            "FUC" => Self::FerrovieUdineCividale,
            "SBB" => Self::SwissFederalRailways,
            _ => Self::Unknown(raw_carrier),
        }
    }
}

impl From<(String, String, bool)> for TrainTripStop {
    fn from(stop: (String, String, bool)) -> Self {
        let time_pieces = stop
            .1
            .split(":")
            .map(|x| x.parse::<u8>().unwrap())
            .collect::<Vec<_>>();
        Self {
            station_name: stop.0,
            time: std::time::Duration::from_secs(
                time_pieces[0] as u64 * 3600 + time_pieces[1] as u64 * 60,
            ),
            reduced_mobility: stop.2,
        }
    }
}

#[derive(Debug)]
struct RFIEdge {
    arrival_index: usize,
    departure: std::time::Duration,
    arrival: std::time::Duration,
    carrier: Carrier,
    train_number: TrainNumber,
}

pub struct RFI {
    trips: Vec<TrainTrip>,
}

impl RFI {
    pub fn new() -> Self {
        let json_contents_gz = include_bytes!("../../target/trains.json.gz");
        let json_bytes = inflate::inflate_bytes(json_contents_gz).unwrap();
        let json_contents = std::str::from_utf8(&json_bytes).unwrap();
        let trains: std::collections::HashMap<String, RawTrainTrip> =
            serde_json::from_str(json_contents).unwrap();
        Self {
            trips: trains.into_values().map(|x| x.into()).collect(),
        }
    }
    pub fn get_graph(&self) -> RFIGraph {
        let mut station_id_map = std::collections::HashMap::new();
        let mut next_station_id = 0;
        let mut lookup = vec![];
        let mut edges: Vec<Vec<RFIEdge>> = vec![];
        for trip in &self.trips {
            for stop in &trip.stops {
                if station_id_map
                    .get(&(stop.station_name.clone(), stop.reduced_mobility))
                    .is_none()
                {
                    station_id_map.insert(
                        (stop.station_name.clone(), stop.reduced_mobility),
                        next_station_id,
                    );
                    lookup.push((stop.station_name.clone(), stop.reduced_mobility));
                    next_station_id = lookup.len();
                }
            }
            edges.resize_with(next_station_id, Default::default);
            for (prev, cur) in trip.stops.iter().zip(trip.stops.iter().skip(1)) {
                let prev_idx = *station_id_map
                    .get(&(prev.station_name.clone(), prev.reduced_mobility))
                    .unwrap();
                let cur_idx = *station_id_map
                    .get(&(cur.station_name.clone(), cur.reduced_mobility))
                    .unwrap();
                edges[prev_idx].push(RFIEdge {
                    arrival_index: cur_idx,
                    departure: prev.time,
                    arrival: cur.time,
                    carrier: trip.carrier.clone(),
                    train_number: trip.number.clone(),
                });
            }
        }

        RFIGraph { lookup, edges }
    }
}

#[derive(Debug)]
pub struct RFIGraph {
    lookup: Vec<(String, bool)>,
    /// Every node contains the index, time of departure in seconds, carrier name and train number
    edges: Vec<Vec<RFIEdge>>,
}

impl RFIGraph {
    pub fn get_graphviz_repr(&self) -> String {
        let mut seen = std::collections::HashSet::new();
        let a = self
            .edges
            .iter()
            .enumerate()
            .map(|(i, x)| (&self.lookup[i].0, x.iter()))
            .map(|(x, i)| std::iter::repeat(x).zip(i))
            .flatten()
            .map(|x| (x.0, &self.lookup[x.1.arrival_index].0))
            .map(|x| format!("\"{}\" -> \"{}\";", x.0, x.1))
            .filter(|x| {
                if !seen.contains(x) {
                    seen.insert(x.clone());
                    true
                } else {
                    false
                }
            })
            .collect::<Vec<_>>();
        format!("digraph{{\n{}}}", a.join("\n"))
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let rfi = super::RFI::new();
        std::fs::write("rfi.dot", rfi.get_graph().get_graphviz_repr()).unwrap();
    }
}
